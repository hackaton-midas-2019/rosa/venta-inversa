Venta inversa:
    Ésta idea surge para solucionar esos problemas donde una persona quiere encontrar un producto, que por sus características es díficil de hallar en el mercado, 
    y no son suficientes las plataformas vigentes de venta para cubrir tal necesidad.
    
Nombre proyecto: Consiguelo

Integrantes:
    Antonucci, Germán.
    Campos, Enrique.
    Gonzáles, Nicolás.
    Pagano, Pablo.
    Saponara, Martín.